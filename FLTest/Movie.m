//
//  Movie.m
//  Created by Tao Yang on 24/06/14.
//  Copyright (c) 2013 Tao Yang. All rights reserved.////

#import "Movie.h"

@implementation Movie

@synthesize name;
@synthesize start_time;
@synthesize end_time;
@synthesize channel;
@synthesize rating;

@end
